from django.urls import path,include
from .views import TownsList,TownsDetail
urlpatterns = [
path('',TownsList.as_view(),name="towns"),
path('<int:pk>/',TownsDetail.as_view(),name='towndetail')
]