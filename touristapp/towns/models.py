from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.

class town(models.Model):
    name=models.CharField(max_length=30,unique=True)
    summary=models.TextField(max_length=200,blank=True)
    image = models.ImageField(blank=True)
    body = RichTextUploadingField(blank=True)

    def __str__(self):
        return self.name


class resturant(models.Model):
    name =models.CharField(max_length=40,unique=True)
    summary=models.TextField(max_length=200,blank=True)
    image = models.ImageField(blank=True)
    link = models.URLField(blank=True)
    town = models.ForeignKey(town,on_delete=models.CASCADE,blank=True)
    def __str__(self):
        return self.name


class events(models.Model):
    name =models.CharField(max_length=40,unique=True)
    summary=models.TextField(max_length=200,blank=True)
    image = models.ImageField(blank=True)
    link = models.URLField(blank=True)
    town = models.ForeignKey(town,on_delete=models.CASCADE,blank=True)

    def __str__(self):
        return self.name





class places(models.Model):
    class placestosee(models.TextChoices):
        FORT = 'FR', ('FORT')
        SEE = 'SE', ('SEE')
        HILLS = 'HL', ('HILLS')


    name =models.CharField(max_length=40,unique=True)
    summary=models.TextField(max_length=200,blank=True)
    image = models.ImageField(blank=True)
    body = RichTextUploadingField(blank=True)
    type=models.CharField(
        max_length=2,
        choices=placestosee.choices,
        default=None,
    )
    town = models.ForeignKey(town,on_delete=models.CASCADE,blank=True)
    
    
    def __str__(self):
        return self.name

