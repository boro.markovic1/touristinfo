from django.shortcuts import render,get_object_or_404
from django.views.generic import *
from .models import *
from django.urls import reverse

# Create your views here.
def index(request):
    return render(request,'../templates/home.html')


class TownsList(ListView):
    model= town
    template_name = '../templates/towns.html'

    def get(self, request, *args, **kwargs):
        towns = town.objects.all()
        return render(request, self.template_name, {'towns': towns})


class TownsDetail(DetailView):
    model = town
    template_name = '../templates/towndetail.html'

    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        article = get_object_or_404(town, pk=pk)

        return render(request, self.template_name, {'article': article})



class RestaurantList(ListView):
    model = resturant
    template_name = '../templates/restaurant.html'
    
    def get(self, request, *args, **kwargs):
        restaurants = resturant.objects.all()
        return render(request, self.template_name, {'restauratns': restaurants})


class EventList(ListView):
    model = events
    template_name = '../templates/events.html'
    
    def get(self, request, *args, **kwargs):
        event = events.objects.all()
        return render(request, self.template_name, {'events': event})

class PlacesList(ListView):
    model = places
    template_name = '../templates/places.html'
    
    def get(self, request, *args, **kwargs):
        place = places.objects.all()
        print(place)
        return render(request, self.template_name, {'places': place})

def placesbytype(request,type):
    place = places.objects.filter(type=type)
    print(place)
    context = {
        'place':place}
    return render(request,'../templates/placesbytype.html',context)



